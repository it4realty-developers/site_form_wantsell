<?php

/**
 *  настроечная форма
 */
function site_form_wantrent_admin_form(){
  
  //получем перечень языков на сайте для frontend
  $site_languages = pr_common_f_langlist_frontend();

  // тема письма посетителю
  foreach ($site_languages as $langcode => $lang_value) {
    
    $form['container_'.$langcode] = array(
      '#type' => 'container',
      '#title' => t('it4r_all_admform_visitor'),
    );
	
	//справочный текст
	$form['container_'.$langcode]['it4r_wantrent_admform_descr_'.$langcode] = array(
	  '#type' => 'textfield',
	  '#title' => t('it4r_wantrent_admform_descr').' ('.$lang_value->name.')',
	  '#default_value' => variable_get('it4r_wantrent_admform_descr_'.$langcode, '')
	);
      
	//сообщение после отправки письма
	$form['container_'.$langcode]['it4r_wantrent_admform_message_'.$langcode] = array(
	  '#type' => 'textfield',
	  '#title' => t('it4r_wantrent_admform_message').' ('.$lang_value->name.')',
	  '#default_value' => variable_get('it4r_wantrent_admform_message_'.$langcode, '')
	);
	
	//филдсет настройки сообщение для поситителя сайта
	$form['container_'.$langcode]['form_visitor_'.$langcode] = array(
	  '#type' => 'fieldset',
	  '#title' => t('it4r_all_admform_visitor').' ('.$lang_value->name.')',
	  '#collapsible' => TRUE,
	  '#collapsed' => TRUE,
	); 
	
	//тема письма для получателя
	$form['container_'.$langcode]['form_visitor_'.$langcode]['it4r_wantrent_admrequest_visitorsubject_'.$langcode] = array(
      '#type' => 'textfield',
      '#title' => t('it4r_all_admform_subject').' ('.$lang_value->name.')',
      '#default_value' => variable_get('it4r_wantrent_admrequest_visitorsubject_'.$langcode, '')
    );
	
	//тело письма для получателя
	$visitor_body = variable_get('it4r_wantrent_admrequest_visitorbody_'.$langcode, '');
	$form['container_'.$langcode]['form_visitor_'.$langcode]['it4r_wantrent_admrequest_visitorbody_'.$langcode] = array(
	  '#type' => 'text_format',
	  '#title' => t('it4r_all_admform_body').' ('.$lang_value->name.')',
	  '#default_value' => (isset($visitor_body['value']) ? $visitor_body['value'] : ''), 
	);
	
	//филдсет настройки сообщение для получателя
	$form['container_'.$langcode]['form_recipient_'.$langcode] = array(
	  '#type' => 'fieldset',
	  '#title' => t('it4r_all_admform_recipient').' ('.$lang_value->name.')',
	  '#collapsible' => TRUE,
	  '#collapsed' => TRUE,
	);
  
	//получатель
	$option_recipient = siteFormsApiBase::getFormRecipient();
	$form['container_'.$langcode]['form_recipient_'.$langcode]['it4r_wantrent_admform_recipientuid_'.$langcode] = array(
	  '#type' => 'select',
	  '#title' => t('it4r_all_admform_recipient').' ('.$lang_value->name.')',
	  '#options'=> $option_recipient,
	  '#default_value' => variable_get('it4r_wantrent_admform_recipientuid_'.$langcode, ''),
	);
  
	//тема письма для администратору
	$form['container_'.$langcode]['form_recipient_'.$langcode]['it4r_wantrent_admform_recipientsubject_'.$langcode] = array(
	  '#type' => 'textfield',
	  '#title' => t('it4r_all_admform_subject').' ('.$lang_value->name.')',
	  '#default_value' => variable_get('it4r_wantrent_admform_recipientsubject_'.$langcode, ''), 
	);
        
	//тело письма для администратору
	$recipient_body = variable_get('it4r_wantrent_admform_recipientbody_'.$langcode, '');
	$form['container_'.$langcode]['form_recipient_'.$langcode]['it4r_wantrent_admform_recipientbody_'.$langcode] = array(
	  '#type' => 'text_format',
	  '#title' => t('it4r_all_admform_body').' ('.$lang_value->name.')',
	  '#format' => 'filtered_html',
	  '#default_value' => (isset($recipient_body['value']) ? $recipient_body['value'] : ''), 
	);
	
  }
  
  return system_settings_form($form);
    
}