<?php

/**
 * Форма
 */
function site_form_wantsell_form($form, &$form_state) {
  
  //филдсет
  $form['forms_api_wantsale_contact_form'] = array(
	'#type' => 'container',
  );
    
    //описание для формы
    $form['forms_api_wantsale_contact_form']['wantsale_markup'] = array(
        '#type'=> 'item',
        '#markup'=> t(site_forms_api_f_constr_get_desr('forms_api_constr_wantsale')),
    );
    
    //имя
    $form['forms_api_wantsale_contact_form']['wantsale_name'] = array(
        '#type' => 'textfield',
        '#title' => t('What is your name'),
        '#required' => TRUE,
    );
    
    //почта
    $form['forms_api_wantsale_contact_form']['wantsale_email'] = array(
        '#type' => 'textfield',
        '#title' => t('Your e-mail'),
        '#required' => TRUE,
    );

    //телефон
    $form['forms_api_wantsale_contact_form']['wantsale_phone'] = array(
        '#type' => 'textfield',
        '#title' => t('Your phone'),
        '#required' => TRUE,
    );
    
    //где вы находитесь
    $form['forms_api_wantsale_contact_form']['wantsale_your_location'] = array(
        '#type' => 'textfield',
        '#title' => t('Your location'),
    );
    
    //филдсет
    $form['forms_api_wantsale_object_form'] = array(
        '#type' => 'fieldset',
        '#title' => t('Information about your site'),
    );
    
    $sale_tid = property_api_get_tid_by_code(variable_get('pr_property_api_sale_code', 'sale'));
    $type_tree = pr_common_f_api_taxonomy_tree('property_type','',$sale_tid);
    $options_type = array();
    for($i=0,$count = count($type_tree); $i<$count; $i++){
        $options_type[$type_tree[$i]['tid']] = $type_tree[$i]['name'];
    }
    //тип недвижимости
    $form['forms_api_wantsale_object_form']['wantsale_type'] = array(
       '#type' => 'select',
       '#title' => t('Type'),
       '#options' => $options_type,
    );
    
    $location_tree = pr_common_f_api_taxonomy_tree('location');
    $options_location = array();
    for($i=0,$count = count($location_tree); $i<$count; $i++){
        $options_location[$location_tree[$i]['tid']] = $location_tree[$i]['name'];
    }
    //месторасположение
    $form['forms_api_wantsale_object_form']['wantsale_location'] = array(
       '#type' => 'select',
       '#title' => t('Location'),
       '#multiple' => TRUE, 
       '#options' => $options_location,
    );
    
    //цена
    $form['forms_api_wantsale_object_form']['wantsale_price'] = array(
        '#type' => 'textfield',
        '#title' => t('Price'),
    );
    
    //валюта
    $option_currency = pr_price_f_api_currency();
    $form['forms_api_prop_request_rent_form']['wantsale_currency'] = array(
       '#type' => 'select',
       '#title' => t('Currency'),
       '#options' => $option_currency,
    );
    
    //описание
    $form['forms_api_proprequest_form']['wantsale_descr'] = array(
        '#type' => 'textarea',
        '#title' => t('Note on request'),
    );
    
    //филдсет
    $form['forms_api_wantsale_images_form'] = array(
        '#type' => 'fieldset',
        '#title' => t('Information about your object'),
    );
    
    //изображение
    $form['forms_api_wantsale_images_form']['wantsale_file'] = array(
        '#type' => 'file',
        '#name' => 'files[]',
        '#title' => t('Images'),
        '#description' => t('Maximum file size: <strong>10 МБ</strong>.<br>
                             Allowed file types: <strong>png gif jpg jpeg</strong>.<br>
                             Image size should be between <strong>150x150</strong> and <strong>1500x1500</strong> pixels.'),
        '#attributes' => array('multiple' => 'multiple'),
    );
    
    //recapcha
    $form['captcha'] = array(
      '#type'=> 'item',
      '#markup'=>''
    );
    
    $form['wantsale_submit'] = array(
        '#type' => 'submit',
        '#value' => t('Send'),
        '#weight' => 200,
    );
    
    return $form;
  
}

/**
 * Submit формы
 */
function site_form_wantsell_form_submit($form, &$form_state) {
  
//  global $language;
//  $langcode = $language->language;
//  
//  $values = $form_state['values'];
//  
//  $forms_api = new siteFormsApiBase();
//  
//  //сформируем html отображение данных формы
//  $human_view = site_form_wantrent_human_view($values);
//  
//  $recipient_uid = variable_get('it4r_property_admform_recipientuid_'.$langcode, '');
//  // добавление в заполненные формы
//  $data = array(
//	'form_type' => 'property_request',
//	'form_recipient' => $recipient_uid,
//	'sender_email' => $values['property_request_email'],
//	'sender_name' => $values['property_request_name'],
//	'sender_phone' => $values['property_request_phone'],
//	'sender_file' => '',
//	'human_view' => $human_view, 
//	'form_serialize' => $values
//  );
//  $forms_api->addForm($data);
//  
//  $form_info = '';
//  
//  if(!empty($values['property_request_name'])){
//	$form_info .= '<p>';
//	  $form_info .= '<label>'.t('it4r_all_admform_name').': '.'</label>';
//	  $form_info .= '<span>'.$values['property_request_name'].'</span>';
//	$form_info .= '</p>';
//  }
//  
//  if(!empty($values['property_request_email'])){
//	$form_info .= '<p>';
//	  $form_info .= '<label>'.t('it4r_all_admform_email').': '.'</label>';
//	  $form_info .= '<span>'.$values['property_request_email'].'</span>';
//	$form_info .= '</p>';
//  }
//  
//  if(!empty($values['property_request_phone'])){
//	$form_info .= '<p>';
//	  $form_info .= '<label>'.t('it4r_all_admform_phone').': '.'</label>';
//	  $form_info .= '<span>'.$values['property_request_phone'].'</span>';
//	$form_info .= '</p>';
//  }
//  
//  // отправка посетителю сайта
//  $visitor_body = variable_get('it4r_property_admrequest_visitorbody_'.$langcode, '');
//  
//  $visitor_body_letter = '';
//  if(isset($visitor_body['value'])){
//	$visitor_body_letter .= $visitor_body['value'];
//  }
//  $visitor_body_letter .= $form_info;
//  $visitor_body_letter .= $human_view;
//  
//  $forms_api->sendMail(
//	'property_request', 
//	$values['property_request_name'].' '.'<'.$values['property_request_email'].'>', 
//	variable_get('site_name').' '.'<'.variable_get('site_mail').'>', 
//	array(
//	  'subject' =>  variable_get('it4r_property_admrequest_visitorsubject_'.$langcode, ''),
//	  'body' => $visitor_body_letter
//	)
//  );
//  
//  //отправка получателю сайта
//  //если заполненный получатель в настройках
//  if(is_numeric($recipient_uid)){
//	$recipient_body = variable_get('it4r_property_admform_recipientbody_'.$langcode, '');
//	$recipient_user = user_load($recipient_uid);
//	
//	$recipient_body_letter = '';
//	if(isset($recipient_body['value'])){
//	  $recipient_body_letter .= $recipient_body['value'];
//	}
//	$recipient_body_letter .= $form_info;
//	$recipient_body_letter .= $human_view;
//	// отправка посетителю сайта
//	$forms_api->sendMail(
//	  'property_request', 
//	  $recipient_user->name.' '.'<'.$recipient_user->mail.'>', 
//	  variable_get('site_name').' '.'<'.variable_get('site_mail').'>', 
//	  array(
//		'subject' => variable_get('it4r_property_admform_recipientsubject_'.$langcode, ''),
//		'body' => $recipient_body_letter
//	  )
//	);
//  }
//  
//  //если есть данные о сообщении на сайт 
//  $message_site = variable_get('it4r_property_admform_message_'.$langcode, '');
//  if($message_site != ''){
//	drupal_set_message($message_site);
//  }
//  
  
}

function site_form_wantsell_human_view($form_param){
  
  $output = '';
  
  //заметки к объекту
  if(!empty($form_param['property_request_note'])){
	
	//получаем id объекта недвижимости
    $property_lot = prProperty::NidToLot($form_param['property_request_node_nid']);
    $link = l('Загловок ноды пока заглушка'.' ('.$property_lot.')',
			  'node/'.$form_param['property_request_node_nid']); 
	//запишем ссылку на объект
	$output .= '<p>';
	  $output .= '<label>'.t('it4r_property_messageform_link').': </label>';
	  $output .= '<span>'.$link.'</span>';
	$output .= '</p>';
	//данные заметки заполененной пользователем
	$output .= '<p>';
	  $output .= '<label>'.t('it4r_property_messageform_note').': </label>';
	  $output .= '<span>'.$form_param['property_request_note'].'</span>';
	$output .= '</p>';
  }

  return $output;
  
}











































/**
 * Функция для переработки данных форм в человеческое отображение
 * 
 * @param type $data - массив из данных заполненных форм
 */
function forms_api_constr_proprent_human2($data){
    
    $output = '';
    
    //местоположение
    if(!empty($data['proprent_location'])){
        $output .= '<p>';
            $output .= '<label>'.t('Locality').': </label>';
            $output .= '<span>'.implode(', ', pr_common_api_f_term_name($data['proprent_location'])).'</span>';
        $output .= '</p>';
    }
    
    //типы недвижимости
    if(!empty($data['proprent_type'])){
        $output .= '<p>';
            $output .= '<label>'.t('Property Type').': </label>';
            $output .= '<span>'.pr_common_api_f_term_name($data['proprent_type']).'</span>';
        $output .= '</p>';
    }
    
    //цена от и до
    if(!empty($data['proprent_price_from']) || !empty($data['proprent_price_to'])){
        $output .= '<p>';
            $output .= '<label>'.t('Price').': </label>';
            if(!empty($data['proprent_price_from'])){
                $output .= '<span>'.t('from').' '. $data['proprent_price_from'] . '</span> ';
            }
            if(!empty($data['proprent_price_to'])){
                $output .= '<span>'.t('to') .' '. $data['proprent_price_to'].'</span>';
            }
            if(!empty($data['proprent_currency'])){
                $output .= '<span> ' . $data['proprent_currency'].'</span>';
            }
        $output .= '</p>';
    }
    
    //площадь от и до
    if(!empty($data['proprent_square_from']) || !empty($data['proprent_square_to'])){
        $output .= '<p>';
            $output .= '<label>'.t('Area').': </span>';
            if(!empty($data['proprent_square_from'])){
                $output .= '<span>'.t('to').' '. $data['proprent_square_from'] . '</span> ';
            }
            if(!empty($data['proprent_square_to'])){
                $output .= '<span>'.t('from').' '. $data['proprent_square_to'].'</span>';
            }
            if(!empty($data['proprent_square_to']) || !empty($data['proprent_square_from'])){
                $output .= ' '.t('m²');
            }
        $output .= '</p>';
    }
    
    return $output;
}